<?php
namespace pt1c\parser;

use Exception;
use SimpleXMLElement;

class XmlParser
{

    private $xmlFilePath;
    private $xmlData;
    private $parsedArray;
    private $ignoreArray;

    public function __construct($xmlFilePath, $ignoreFilePath)
    {
        $this->parsedArray = array();
        $this->xmlFilePath = $xmlFilePath;
        $this->readXmlFile($xmlFilePath);

        $this->ignoreArray = array();
        $this->readIgnoreFile($ignoreFilePath);
    }

    public function checkSimpleXML(){
        if (extension_loaded('SimpleXML'))
            return true;
        return false;
    }

    public function parseGoods(){
        if(empty($this->xmlData))
            $this->readXmlFile($this->xmlFilePath);

        $xml = new SimpleXMLElement($this->xmlData);

        $goodarr = array();
        $qtyVariants = ['many','lots'];

        foreach ($xml->Goods as $good) {
            unset($goodarr);
            $goodarr['name'] = (string)$good->Name;
            $goodarr['article'] = (string)$good->Article;
            $goodarr['code'] = (string)$good->Code;
            $goodarr['price'] = (string)$good->Price;

            if(in_array((string)$good->Quantity, $qtyVariants) or  (int)$good->Quantity>0){
                $goodarr['qty'] = (int)1;
            } else { //empty or 0 or other variants
                $goodarr['qty'] = (int)0;
            }

            $goodarr['ignore'] = false;
            if($this->checkIgnoreArray()){
                if(isset($this->ignoreArray[$goodarr['article']])){
                    if($this->ignoreArray[$goodarr['article']] == 'all')
                        $goodarr['ignore'] = true;
                    else
                        $goodarr['ignore'] = strtolower($this->ignoreArray[$goodarr['article']]);
                }
            }

            $this->parsedArray[] = $goodarr;
        }

        unset($goodarr);
    }

    public function readXmlFile($path){
        if(empty($path))
            $path = $this->xmlFilePath;

        if(!$this->xmlData = file_get_contents($path))
        {
            //throw new Exception('Load Failed');
            echo "File not loaded. Check path!\n";
            exit();
        }
    }

    public function getAllGoods(){
        return $this->parsedArray;
    }

    public function readIgnoreFile($path){
        $handle = @fopen($path, "r");
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                $exploded = explode(";",trim($buffer));

                if(!isset($exploded[1]))
                    $exploded[1] = "all";

                $this->ignoreArray[$exploded[0]] = $exploded[1];
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);
        } else {
            unset($this->ignoreArray);
        }
    }

    public function getIgnore(){
        if($this->checkIgnoreArray()){
            return $this->ignoreArray;
        }
        return false;
    }

    public function checkIgnoreArray(){
        if(isset($this->ignoreArray)){
            return true;
        }
        return false;
    }
}
