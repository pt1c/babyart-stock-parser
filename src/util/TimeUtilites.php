<?php

namespace pt1c\util;

use DateTime;
Use DateTimeZone;

class TimeUtilites
{

    public static $start;
    public static $working_time;

    public static function start(){
        TimeUtilites::$start = microtime(true);
    }

    public static function end(){
        TimeUtilites::$working_time = microtime(true) - TimeUtilites::$start;
    }

    public static function getWorkingTime(){
        return round(TimeUtilites::$working_time, 3);
    }

    public static function now(){
        $date = new DateTime('NOW');
        //$date->setTimezone(new DateTimeZone('Europe/Moscow'));
        return $date->format('d.m.Y H:i:s');
    }
}