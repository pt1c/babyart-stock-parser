<?php

namespace pt1c\db;

use PDO;
use PDOException;

class MysqlAdapter
{
    private $host;
    private $port;
    private $name;
    private $username;
    private $password;
    private $charset;
    private $pdo;

    public function __construct($host='127.0.0.1',
                                $port='3306',
                                $name,
                                $username,
                                $password,
                                $charset='utf8')
    {
        $this->host = $host;
        $this->port = $port;
        $this->name = $name;
        $this->username = $username;
        $this->password = $password;
        $this->charset = $charset;
    }

    public function connect(){
        try{
            $this->pdo = new PDO(
                sprintf(
                    'mysql:host=%s;dbname=%s;port=%s;charset=%s',
                    $this->host,
                    $this->name,
                    $this->port,
                    $this->charset
                ),
                $this->username,
                $this->password
            );
        } catch (PDOException $e) {
            echo "Database connection failed. Check config!";
            exit();
        }
    }

    public function isRowExist($tabName, $art){
        $sql='SELECT id FROM '.$tabName.' WHERE art=:article';
        $statement = $this->pdo->prepare($sql);
        $statement->bindValue(':article',$art, PDO::PARAM_STR);
        if ($statement->execute() && $statement->rowCount() > 0)
            return true;

        return false;
    }

    public function updateRow($tabName, $params, $key=''){

        switch($key){
            case "p": //price only
                $sql_set_part = "price=:price";
                break;

            case "q": //qty only
                $sql_set_part = "presence=:qty";
                break;

            default:
                $sql_set_part = "presence=:qty, price=:price";
                break;
        }

        $sql = 'UPDATE '.$tabName.' SET '.$sql_set_part.' WHERE art=:article';
        $statement = $this->pdo->prepare($sql);

        //недостаточная фильтрация, но больше не надо, т.к. не пользовательский ввод
        $statement->bindValue(':article',$params['article'], PDO::PARAM_STR);
        if($key == "p" or empty($key)) $statement->bindValue(':price',$params['price'], PDO::PARAM_INT);
        if($key == "q" or empty($key)) $statement->bindValue(':qty',$params['qty'], PDO::PARAM_INT);

        if ($statement->execute())
            return true;
        else
            return false;

    }
}

