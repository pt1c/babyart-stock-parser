<?php

class AutoLoader {

    public static $prefix   = 'pt1c\\';         //namespace prefix
    //public static $basedir  = __DIR__ . '/';    //base directory  >> 5.4
    public static $basedir  = __DIR__;    //base directory
    //public static $cfgPath  = __DIR__ . '/../config/config.php';   //Config php >> 5.4
    public static $cfgPath  = '/../config/config.php';   //Config

    public static function loader($class){
        AutoLoader::loadConfig();

        $len = strlen(AutoLoader::$prefix);
        if (strncmp(AutoLoader::$prefix, $class, $len) !== 0) {
            return;
        }

        $relative_class = substr($class, $len);
        $file = AutoLoader::$basedir . '/' . str_replace('\\', '/', $relative_class) . '.php';
        if (file_exists($file)) {
            require $file;
        }
    }

    public static function loadConfig(){
        $cfg_path = AutoLoader::$cfgPath . '/' . AutoLoader::$cfgPath;
        if (file_exists($cfg_path)) {
            require_once $cfg_path;
        }
    }
}

spl_autoload_register('Autoloader::loader');