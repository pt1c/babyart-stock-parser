<?php
set_time_limit(0);
require_once('config/config.php');
require_once('src/AutoLoader.php');
date_default_timezone_set(TIMEZONE);

use pt1c\parser\XmlParser;
use pt1c\db\MysqlAdapter;
use pt1c\util\CommandLine;
use pt1c\util\TimeUtilites;

$args = CommandLine::parseArgs($_SERVER['argv']);

if(isset($args['p']) and $args['p'] == true){
    $key = "p";
}  elseif (isset($args['q']) and $args['q'] == true) {
    $key = "q";
} else {
    $key = UPDATE_KEY;
}
$outputk = (isset($args['o']) and $args['o'] == true) ? $outputk="yes" : CL_OUTPUT;

if (strtolower($outputk) == "yes") {
    TimeUtilites::start();
    echo "XML Parser v" . VERSION . "\n";
    echo "Start time: " . TimeUtilites::now() . "\n";
    echo "\n";
}

$db = new MysqlAdapter(DB_HOST, DB_PORT, DB_NAME, DB_LOGIN, DB_PASS, DB_CHARSET);
$db->connect();

$xmlParser = new XmlParser(XML_URL, IGNORE_URL);
$xmlParser->parseGoods();
$parsedGoods = $xmlParser->getAllGoods();

foreach ($parsedGoods as $good) {
    if($db->isRowExist(DB_CATALOG_TABLE, $good['article'])){
        if ($good['ignore'] === false) {
            $result = $db->updateRow(DB_CATALOG_TABLE, $good, $key);
        } elseif ($good['ignore'] === 'p' or $good['ignore'] === 'q') {
            $result = $db->updateRow(DB_CATALOG_TABLE, $good, $good['ignore']);
        }

        if (strtolower($outputk) == "yes") {
            if ($good['ignore'] === false) {
                if ($result) echo $good['article'] . " - OK\n";
                else echo $good['article'] . " - FAILED \n";
            } elseif ($good['ignore'] === 'p' or $good['ignore'] === 'q') {
                if ($result) {
                    if ($good['ignore'] === 'p') {
                        echo $good['article'] . " - OK (only PRICE)\n";
                    } elseif ($good['ignore'] === 'q') {
                        echo $good['article'] . " - OK (only QUANTITY)\n";
                    } else {
                        echo $good['article'] . " - KEY ERROR (See ignore.txt)\n";
                    }
                } else {
                    echo $good['article'] . " - FAILED (IGNORED)\n";
                }
            } else {
                echo $good['article'] . " - IGNORED\n";
            }
        }
    } else {
        if (strtolower($outputk) == "yes") echo $good['article'] . " - NOT FOUND\n";
    }
}

if (strtolower($outputk) == "yes") {
    TimeUtilites::end();
    echo "\n";
    echo "Working time: " . TimeUtilites::getWorkingTime() . " seconds \n";
    echo "Author: Kirill [pt1c] Savinov \n";
    echo "Bitbucket: https://bitbucket.org/pt1c/babyart-stock-parser \n";
}
