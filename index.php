<?php
set_time_limit(0);
require_once('config/config.php');
require_once('src/AutoLoader.php');
date_default_timezone_set(TIMEZONE);

use pt1c\parser\XmlParser;
use pt1c\db\MysqlAdapter;
use pt1c\util\TimeUtilites;

$key = isset($_POST['key']) ? $_POST['key'] : UPDATE_KEY;
$outputk = isset($_POST['output']) ? $_POST['output'] : WEB_OUTPUT;

if (strtolower($outputk) == "yes") {
    TimeUtilites::start();
    echo "XML Parser v" . VERSION . "<br  />";
    echo "Start time: " . TimeUtilites::now() . "<br  />";
    echo "<br />";
}

$db = new MysqlAdapter(DB_HOST, DB_PORT, DB_NAME, DB_LOGIN, DB_PASS, DB_CHARSET);
$db->connect();

$xmlParser = new XmlParser(XML_URL, IGNORE_URL);
$xmlParser->parseGoods();
$parsedGoods = $xmlParser->getAllGoods();

foreach ($parsedGoods as $good) {
    if($db->isRowExist(DB_CATALOG_TABLE, $good['article'])){
        if ($good['ignore'] === false) {
            $result = $db->updateRow(DB_CATALOG_TABLE, $good, $key);
        } elseif ($good['ignore'] === 'p' or $good['ignore'] === 'q') {
            $result = $db->updateRow(DB_CATALOG_TABLE, $good, $good['ignore']);
        }

        if (strtolower($outputk) == "yes") {
            if ($good['ignore'] === false) {
                if ($result) echo $good['article'] . " — OK <br />";
                else echo $good['article'] . " — FAILED <br />";
            } elseif ($good['ignore'] === 'p' or $good['ignore'] === 'q') {
                if ($result) {
                    if ($good['ignore'] === 'p') {
                        echo $good['article'] . " — OK (only PRICE)<br />";
                    } elseif ($good['ignore'] === 'q') {
                        echo $good['article'] . " — OK (only QUANTITY)<br />";
                    } else {
                        echo $good['article'] . " — KEY ERROR (See ignore.txt)<br />";
                    }
                } else {
                    echo $good['article'] . " — FAILED (IGNORED)<br />";
                }
            } else {
                echo $good['article'] . " — IGNORED <br />";
            }
        }
    } else {
        if (strtolower($outputk) == "yes") echo $good['article'] . " — NOT FOUND <br />";
    }
}

if (strtolower($outputk) == "yes") {
    TimeUtilites::end();
    echo "<br />";
    echo "Working time: " . TimeUtilites::getWorkingTime() . " seconds <br />";
    echo "Author: Kirill [pt1c] Savinov <br />";
    echo "Bitbucket: <a href=\"https://bitbucket.org/pt1c/babyart-stock-parser \">https://bitbucket.org/pt1c/babyart-stock-parser</a>";
}
