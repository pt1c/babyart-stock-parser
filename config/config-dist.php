<?php
/*
    Путь к файлу XML
    По умолчанию настроен путь к каталогу xml-incoming с именем файла stock.xml
    Допускается использование http/ftp
    define('XML_URL', 'ftp://login:password@site.ru/path/file.xml');
    define('XML_URL', 'http://site.ru/path/file.xml');
*/
define('XML_URL', __DIR__ . '/../xml-incoming/stock.xml');
/*
    Путь к файлу игнорирования
    По умолчанию настроен путь к каталогу xml-incoming с именем файла ignore.txt
*/
define('IGNORE_URL',   __DIR__.'/../xml-incoming/ignore.txt');

/*
    Таблица с каталогом товаров
    можно оставить по умолчанию, если нет предписания по корректировке
*/
define('DB_CATALOG_TABLE', 'bdot_catalog');

/*
    Данные для доступа к БД
    параметры  DB_PORT и DB_CHARSET можно оставить по умолчанию,
    если нет предписания по из корретировке
*/
define('DB_HOST',    'localhost');   // Хост MySQL сервера
define('DB_PORT',    '3306');        // Порт MySQL сервера
define('DB_LOGIN',   '');            // Логин MySQL сервера
define('DB_PASS',    '');            // Пароль MySQL сервера
define('DB_NAME',    '');            // База MySQL сервера
define('DB_CHARSET', 'utf8');        // Кодировка MySQL сервера

/*
    Настройки работы парсера
*/
define('UPDATE_KEY', '');            // Ключ обновления, q - только наличие, p - только цена, пусто - цена и наличие
define('WEB_OUTPUT', 'NO');          // Вывод информации при запуске скрипта через WEB (index.php)
define('CL_OUTPUT', 'NO');           // Вывод информации при запуске скрипта через Command Line (cli.php)
define('TIMEZONE', 'Europe/Moscow'); // Временная зона
define('VERSION', '1.1.0');          // Версия скрипта
