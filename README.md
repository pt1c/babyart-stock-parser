###Название приложения и его описание
Парсер для загрузки информации по товарам для интернет магазина на движке Joomla из XML файла.

###Требования к среде выполнения
На сервере где будет запущено приложение необходимо иметь:
- php версии 5.4 или выше, установленные расширения
  - libxml
  - SimpleXML
  - PDO
  - pdo_mysql
- mysql

###Инструкция по установке
Все файлы приложения копируются в директорию, где будет выполняться приложение.
Необходимо переименовать файл /config/config-dist.php в config.php и отредактировать в нем конфигурационные параметры.
Все параметры описаны в самом файле посредство комментариев.

###Инструкция по использованию
Приложение может работать в двух вариантах: консольный или веб-вариант.

Для запуска консольного варианта (Запуск с изменением цены и наличия и с выводом по умолчанию (указан в файле config.php)):
> $ php cli.php

Ключи для запуска:
    - ключ -o запуск с принудительным выводом
    - ключ -p запуск с обновлением только цены
    - ключ -q запуск с обновлением только наличия

> $ php cli.php -op
> $ php cli.php -o
> $ php cli.php -qo

Для web-варинат необходимо перейти по URL где располагается приложение и запустить файл index.php (Запуск с изменением цены и наличия и с выводом по умолчанию (указан в файле config.php)):
> index.php

Ключи для запуска:
    - ключ output=yes запуск с принудительным выводом
    - ключ key=p запуск с обновлением только цены
    - ключ key=q запуск с обновлением только наличия    

> index.php?output=yes
> index.php?key=q
> index.php?output=yes&key=p

####ignore.txt
В файл записываются артикулы, каждый на новой строке, которые не слудет изменять.
если после артикула через ; записать ключ p или q изменится только цена или количество, соответственно.

###Инструкция другим разработчикам
В приложениии не реализован обработчик ошибок. Все ошибки выводятся непосродственно через echo.
Приложение построено согласно PSR-4.

- /src/db/MysqlAdapter - класс адптера к PDO с инкапсулироваными запросами на изменение БД
- /src/parser/XmlParser - класс описывающий парсер, для предоставленного файла XML
- /src/AutoLoader - загрузчик соответстующий PSR-4

TODO:
- обработка ошибок
- логирование (лучшая реализация через monolog) - не реализовано по причине усложнения установки приложения, требуется composer

###Сведения по поддрежке
Поддержка программного продукта осуществляется в срок и в рамках гарантийныз обязательств.

###Сведения об авторах
Савинов [pt1c] Кирилл — kirill@savinov.me
 
###Лицензия программного обеспечения
Лицензия определяется заказчиком приложения.